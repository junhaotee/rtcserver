package RTCServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class ConnectionHandler implements Runnable {

    public Socket clientSocket;
    BufferedReader in;
    PrintWriter out;
    String userInput;
    String userId;
    JSONArray requestArr;
    JSONObject serverResponse;
    JSONObject friendList;
    JSONObject lastChat;
    JSONObject lastChatDate;
    JSONObject userIP;

    public ConnectionHandler(Socket clientSocketMain) {
        try {
            
            clientSocket = clientSocketMain;
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            
        } catch (IOException e) {
        }
    }

    public void run() {
        try {
            
            while ((userInput = in.readLine()) != null) {
                requestHandler(userInput); //request comes in, pass it to handler
            }
            
        } catch (IOException e) {
        }
    }

    public void addConnection() {
        
        RTCServer.socketMap.put(clientSocket.getRemoteSocketAddress().toString(), clientSocket);
        System.out.println("[" + getTimestamp() + "]New connection from " + clientSocket.getRemoteSocketAddress().toString() + " added to AddressTable");
    
    }

    public String getTimestamp() {
        return new Timestamp(new Date().getTime()).toString();
    }

    public void requestHandler(String userRequest) {

        requestArr = (JSONArray) (JSONValue.parse(userRequest));
        System.out.println(Integer.parseInt(requestArr.get(0).toString()));
        switch (Integer.parseInt(requestArr.get(0).toString())) {//base on opcode determine action
            case 0: {
                authenticateUser();
                break;
            }
            case 1: {
                chatHandshake();
                break;
            }
            case 2: {
                chatC2S();
                break;
            }
            case 3: {
                getActiveUserList();
                System.out.println("[" + getTimestamp() + "]Replied active user list. to ("+userId+") "+clientSocket.getRemoteSocketAddress().toString());
                break;
            }
            case 4: {
                audioHandshake();
                break;
            }
            case 5: {
                audioC2S();
                break;
            }
            case 6: {
                videoHandshake();
                break;
            }
            case 7: {
                videoC2S();
                break;
            }
            case 8: {
                closeConnection();
                break;
            }
            default: {
                System.out.println("Undefined action");
                break;
            }
        }
    }

    public void authenticateUser() {
        try {//authentication successful
            serverResponse = new JSONObject();
            
            if (RTCServer.userPasswd.get(requestArr.get(1).toString()).equals(requestArr.get(2).toString())) {
                userId=requestArr.get(1).toString();
                serverResponse.put("0", "10");
                serverResponse.put("1", "1");
                
                getFriendlist();
                getLastChat();
                getLastChatDate();
                getUserIP();
                
                serverResponse.put("2",friendList.toJSONString());
                serverResponse.put("3",lastChat.toJSONString());
                serverResponse.put("4",lastChatDate.toJSONString());
                serverResponse.put("5",userIP.toJSONString());
                
                System.out.println("[" + getTimestamp() + "]"+clientSocket.getLocalSocketAddress().toString()+" connected to "+clientSocket.getRemoteSocketAddress().toString());
                System.out.println("[" + getTimestamp() + "]Made \"" + requestArr.get(1).toString() + "\" now an active user");
                
                RTCServer.userIpAddr.put(requestArr.get(1).toString(),clientSocket.getRemoteSocketAddress().toString());//uId,ipad:socket
                RTCServer.socketMap.put(requestArr.get(1).toString(), clientSocket);
                
                out.println(serverResponse.toJSONString());
                System.out.println("[" + getTimestamp() + "]Authenticated, reply sent back to"+clientSocket.getRemoteSocketAddress().toString());
                
                System.out.println("[" + getTimestamp() + "]Waiting for connection...");
                
            } else {
                serverResponse.put("0", "10");
                serverResponse.put("1", "0");//0 indicates false login
                out.println(serverResponse.toJSONString());
                
                System.out.println("[" + getTimestamp() + "]Failed to authenticate \"" + requestArr.get(1).toString() + "\"");
                System.out.println("[" + getTimestamp() + "]Handling socket closed \"" + requestArr.get(1).toString() + "\"");
                
                clientSocket.shutdownOutput();
                clientSocket.shutdownInput();
                clientSocket.close();
            }
            
        } catch (Exception ioe) {
            
            serverResponse.put("0", "10");
            serverResponse.put("1", "0");
            out.println(serverResponse.toJSONString());
            
            System.out.println("[" + getTimestamp() + "]Failed to authenticate \"" + requestArr.get(1).toString() + "\"");
            System.out.println("[" + getTimestamp() + "]Handling socket closed \"" + requestArr.get(1).toString() + "\"");
            ioe.printStackTrace();
            
            try {
                clientSocket.shutdownOutput();
                clientSocket.shutdownInput();
                clientSocket.close();
            } catch (IOException ex) {
                Logger.getLogger(ConnectionHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void chatHandshake() {
        
    }

    public void chatC2S() {
    }

    public void getActiveUserList() {
        //userIP=new JSONObject(RTCServer.userIpAddr);
        JSONObject activeUserList=new JSONObject();
        activeUserList.put("0","13");
        activeUserList.put("1",RTCServer.userIpAddr.toString());
        out.println(activeUserList.toJSONString());
    }

    public void audioHandshake() {
    }

    public void audioC2S() {
    }

    public void videoHandshake() {
    }

    public void videoC2S() {
    }

    public void closeConnection() {
        
        System.out.println("[" + getTimestamp() + "]Received close socket signal from "+userId);
        try{
            in.close();
            out.close();
            clientSocket.close();
            RTCServer.userIpAddr.remove(userId);
            RTCServer.socketMap.remove(userId);
            
            System.out.println("[" + getTimestamp() + "]Socket closed, and removed user("+userId +") from active user list");

        }catch(IOException ioe){
            System.out.println("Error occured in closeConnection() by ioe");
            ioe.printStackTrace();
        }
    }

    public void replyUnreachablePeer() {
    }
    
    public void getFriendlist(){
        friendList=new JSONObject();//uId,name
        friendList.put("1","鄭俊豪");
        friendList.put("2","李偉俊");
        friendList.put("3","葉子炎");
        friendList.put("4","劉偉量");
        friendList.put("5","蔡宗修");
        friendList.put("6","鄭懷勇");
        friendList.put("7","蘇評威");
        friendList.put("8","陳勁龍");
        friendList.put("9","賀祐農");
        friendList.put("10","吳朝暉");
        friendList.remove(userId);
    }

    public void getLastChat(){
        lastChat=new JSONObject();//uId,chatList
        lastChat.put("1","好吧");
        lastChat.put("2","沒錯，謝啦～");
        lastChat.put("3","不好意思，下課睡著了....=.=");
        lastChat.put("4","okok");
        lastChat.put("5","嗯嗯");
        lastChat.put("6","謝謝！");
        lastChat.put("7","no problem");
        lastChat.put("8","okay thanks");
        lastChat.put("9","");
        lastChat.put("10","you're not alone");
        lastChat.remove(userId);
    }
    
    public void getLastChatDate(){
        lastChatDate=new JSONObject();
        lastChatDate.put("1","2015-12-01 23:59:00");
        lastChatDate.put("2","2015-11-03 21:50:03");
        lastChatDate.put("3","2015-12-04 10:59:10");
        lastChatDate.put("4","2015-12-12 08:59:50");
        lastChatDate.put("5","2015-12-12 13:19:30");
        lastChatDate.put("6","2015-12-14 04:19:00");
        lastChatDate.put("7","2015-12-01 23:59:00");
        lastChatDate.put("8","2015-12-07 11:39:00");
        lastChatDate.put("9","2015-12-25 15:09:57");
        lastChatDate.put("10","2015-12-13 19:11:20");
        lastChat.remove(userId);
    }
    public void getUserIP(){
        userIP=new JSONObject(RTCServer.userIpAddr);
    }

}
