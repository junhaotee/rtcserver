package RTCServer;

import java.net.*;
import java.io.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class RTCServer extends Thread
{
   private ServerSocket serverSocket;
   public static LinkedList<Socket> socketList=new LinkedList<Socket>();
   public static Map<String,Socket> socketMap=new HashMap<String,Socket>();
   public static Map<String,String> userIpAddr=new HashMap<String,String>();
   public static Map<String,String> userPasswd=new HashMap<String,String>();
   public static Date date;
   
   public RTCServer(){
   try{
       generateFakeUsers();
       
        int portNum=5000;
        serverSocket = new ServerSocket(portNum);
        
        System.out.println("["+getTimestamp()+"]Chatserver Started");
        System.out.println("["+getTimestamp()+"]Server Socket created at TCP:"+portNum);
        System.out.println("["+getTimestamp()+"]Waiting for connection...");
   }catch(IOException IOE){}}

   public void run(){
      while(true){
      try{
          
          Socket clientSocket = serverSocket.accept();
          System.out.println("["+getTimestamp()+"]Connection accepted from "+clientSocket.getRemoteSocketAddress().toString());
          new Thread(new ConnectionHandler(clientSocket)).start();
          //new Thread(new KeepAliveConnection()).start(); because when app dead it notifies server, peridically check isn't needed
          
        }catch(IOException e){
           e.printStackTrace();
           break;
        }
      }
   }
   
   public String getTimestamp(){
       return new Timestamp(new Date().getTime()).toString();
   }
   
   public static void main(String [] args){
        Thread t = new RTCServer();
        t.start();
        //this thread continuously check for dead connection, if dead eliminate the data from socketMap
   }
   
   public final void generateFakeUsers(){
        userPasswd.put("1","00000");
        userPasswd.put("2","00000");
        userPasswd.put("3","00000");
        userPasswd.put("4","00000");
    }
}